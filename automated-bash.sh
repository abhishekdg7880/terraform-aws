#!/bin/bash

BUCKET_NAME=$1
KEY=$2
region=$3
ASG_NAME=$4

dd if=web-content/index.html of=web-content/new_index.html conv=ucase
rm -f web-content/index.html
mv web-content/new_index.html web-content/index.html
aws s3 cp web-content/index.html s3://$BUCKET_NAME/$KEY/  --region $region
aws autoscaling start-instance-refresh --auto-scaling-group-name $ASG_NAME --preferences '{"InstanceWarmup": 300, "MinHealthyPercentage": 50}' --region $region