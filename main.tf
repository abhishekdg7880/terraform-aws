locals {
  local_count     = 3
  azs             = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  region          = "ap-southeast-1"
  tags = {
    CreatedBy   = "Terraform"
  }
}

#Deploy Security Groups/Private Subnets 
module "networking" {
  source                   = "./modules/networking"
  local_count              = local.local_count
  vpc_cidr_block           = var.vpc_cidr_block
  vpc_id                   = var.vpc_id
  alb_sg_name              = var.alb_sg_name
  bastion_instance_sg_name = var.bastion_instance_sg_name
  bastion_sg_cidr_block    = var.bastion_sg_cidr_block
  webservers_sg_name       = var.webservers_sg_name
  azs                      = local.azs
  region                   = local.region
  tags                     = local.tags
}

#Deploy S3 Bucket to store HTML Contents
module "block_storage" {
    source           = "./modules/block_storage"
    s3_bucket_name   = var.s3_bucket_name
    s3_bucket_key    = var.s3_bucket_key
    s3_object_upload = var.s3_object_upload
    aws_account_id   = var.aws_account_id
    vpce_id          = module.networking.aws_s3_vpc_endpoint_id
    tags             = local.tags
}

# Deploy Instance IAM Role-Policy
module "iam" {
  source                      = "./modules/iam"
  instance_role_name          = var.instance_role_name  
  instance_profile_role_name  = var.instance_profile_role_name
  s3_bucket_name              = module.block_storage.aws_website_bucket_name
  iam_policy_name             = var.iam_policy_name
  tags                        = local.tags
}


#Deploy AutoScaling Group for Webservers
module "autoscaling_group" {
  source                          = "./modules/autoscaling_group"
  local_count                     = local.local_count
  asg_name                        = var.asg_name
  asg_instance_name               = var.asg_instance_name
  asg_ebs_device_name             = var.asg_ebs_device_name
  asg_ebs_volume_size             = var.asg_ebs_volume_size
  asg_ebs_volume_type             = var.asg_ebs_volume_type
  asg_instance_ami_id             = var.asg_instance_ami_id
  asg_instance_type               = var.asg_instance_type
  asg_instance_key_name           = var.asg_instance_key_name
  lb_target_group_arn             = module.alb.aws_alb_webserver_tg_arn
  ebs_asg_security_groups         = module.networking.aws_webservers_security_group_id
  private_subnet_ids              = module.networking.webserver_private_subnet_ids
  asg_desired_capacity            = var.asg_desired_capacity
  asg_maximum_capacity            = var.asg_maximum_capacity
  asg_minimum_capacity            = var.asg_minimum_capacity
  asg_instance_profile_role_name  = module.iam.aws_webserver_role_name
  tags                            = local.tags
}

#Deploy WebServer Instances and Bastion Host
module "compute" {
    source                          = "./modules/compute"
    bastion_ami                     = var.asg_instance_ami_id
    bastion_instance_type           = var.asg_instance_type
    bastion_key_pair_name           = var.asg_instance_key_name
    bastion_subnet_id               = var.bastion_subnet_id
    bastion_vpc_security_group_ids  = module.networking.aws_bastion_security_group_id
    bastion_volume_size             = var.asg_ebs_volume_size
    bastion_volume_type             = var.asg_ebs_volume_type
    tags                            = local.tags
}

# Deploy Application LoadBalancer Along With Target Groups
module "alb" {
    source                  = "./modules/alb"
    alb_name                = var.alb_name  
    alb_sg_id               = module.networking.aws_alb_security_group_id
    alb_public_subnet_ids   = var.alb_public_subnet_ids
    alb_tg_name             = var.alb_tg_name
    vpc_id                  = var.vpc_id
    tags                    = local.tags
}
