
vpc_cidr_block                  = "172.31.0.0/16"
vpc_id                          = "vpc-a6a862c0"
alb_sg_name                     = "ALBSecurityGroup"
bastion_instance_sg_name        = "BastionNATSg"
bastion_sg_cidr_block           = "0.0.0.0/0"
webservers_sg_name              = "WebServerSg"

s3_bucket_name                  = "apsoutheast-website-bucket"
s3_bucket_key                   = "web-content/index.html"
aws_account_id                  = "784364363154"
s3_object_upload                = "./web-content/index.html"



instance_profile_role_name      = "WebServerInstanceProfileRole"
instance_role_name              = "InstanceS3AccessRole"
iam_policy_name                 = "WebServerS3AccessPolicy"

asg_instance_name               = "WebServerInstanceRefresh"
asg_ebs_device_name             = "/dev/xvda"
asg_ebs_volume_size             = 8
asg_ebs_volume_type             = "gp2"
asg_instance_ami_id             = "ami-0ce792959cf41c394"
asg_instance_type               = "t2.micro"
asg_instance_key_name           = "terraform_key"
asg_desired_capacity            = 3
asg_maximum_capacity            = 3
asg_minimum_capacity            = 3
asg_min_healthy_percentage      = 50
asg_instance_warmup_time        = 300
asg_name                        = "WebServerInstanceASG"


bastion_subnet_id               = "subnet-3d4e2464"

alb_name                        = "WebServerApplicationLoadBalancer"  
alb_public_subnet_ids           = ["subnet-3d4e2464","subnet-0388274b", "subnet-0351ee65"]
alb_tg_name                     = "WebServerTargetGroups"
