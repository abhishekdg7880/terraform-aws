variable "asg_instance_name" {
    type = string
}

variable "asg_ebs_device_name" {
    type = string
    default = "/dev/xvda"
}

variable "asg_ebs_volume_size" {
    type = number
    default = 8
}

variable "asg_ebs_volume_type" {
    type = string
    default = "gp2"
}

variable "asg_instance_ami_id" {
    type = string
}

variable "asg_instance_type" {
    type = string
}

variable "asg_instance_key_name" {
    type = string
}

variable "asg_desired_capacity" {
    type = string
}

variable "asg_maximum_capacity" {
    type = string
}

variable "asg_minimum_capacity" {
    type = string
}

variable "asg_min_healthy_percentage" {
    type = number
}

variable "asg_instance_warmup_time" {
    type = number
}

variable "aws_account_id" {
    type = string
}

variable "s3_bucket_name" {
    type = string
}

variable "iam_policy_name" {
    type = string
}

variable "instance_profile_role_name" {
    type = string
}

variable "vpc_id" {
    type = string
}

variable "vpc_cidr_block" {
    type = string
}

variable "alb_sg_name" {
    type = string
}

variable "bastion_instance_sg_name" {
    type = string
}

variable "bastion_sg_cidr_block" {
    type = string
}

variable "webservers_sg_name" {
    type = string
}

variable "bastion_subnet_id" {
    type = string
}

variable "alb_name" {
    type = string
}

variable "alb_public_subnet_ids" {
    type = list(string)
}

variable "alb_tg_name" {
    type = string
}

variable "s3_bucket_key" {
    type = string
}

variable "s3_object_upload" {
}

variable "instance_role_name" {
    type = string
}

variable "asg_name" {
    type = string
}