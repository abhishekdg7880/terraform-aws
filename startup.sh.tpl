#!/bin/bash


sudo yum update -y
# Install Nginx
sudo amazon-linux-extras install nginx1 -y
# Start Nginx Service
sudo systemctl start nginx
#Remove default nginx HTML page
sudo rm -f /usr/share/nginx/html/index.html
# Change Mode of the Directory
sudo chmod 777 /usr/share/nginx/html/
#Download contents from S3 Bucket
aws s3 sync s3://apsoutheast-website-bucket/web-content/ /usr/share/nginx/html/
