data "aws_iam_policy_document" "instance_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "s3_bucket_access_policy" {
  statement {
    sid = "AccessS3BucketPolicy"

    actions = [
        "s3:GetObjectAcl",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:GetObjectVersion"
    ]

    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}/*",
      "arn:aws:s3:::${var.s3_bucket_name}",
    ]
  }
}

resource "aws_iam_role" "instance_role" {
  name               = var.instance_role_name
  assume_role_policy = data.aws_iam_policy_document.instance_assume_role_policy.json
  tags               = var.tags
}

resource "aws_iam_policy" "instance_policy" {
  name        = var.iam_policy_name
  description = "S3AccessPolicy"
  policy      = data.aws_iam_policy_document.s3_bucket_access_policy.json
  tags        = var.tags
}

resource "aws_iam_role_policy_attachment" "instance_role_policy_attach" {
  role       = aws_iam_role.instance_role.name
  policy_arn = aws_iam_policy.instance_policy.arn
}

resource "aws_iam_instance_profile" "instance_profile_role" {
  name = var.instance_profile_role_name
  role = aws_iam_role.instance_role.name
}
