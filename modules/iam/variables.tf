variable "instance_profile_role_name" {
    type = string
}

variable "s3_bucket_name" {
    type = string
}

variable "iam_policy_name" {
    type = string
}

variable "tags" {
  type = map(string)
}

variable "instance_role_name" {
    type = string
}