variable "s3_bucket_name" {
    type = string
}

variable "aws_account_id" {
    type = string
}

variable "vpce_id" {
    type = string
}

variable "tags" {
    type = map(string)
}

variable "s3_bucket_key" {
    type = string
}

variable "s3_object_upload" {
}
