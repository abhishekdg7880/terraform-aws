resource "aws_lb" "alb_webserver" {
  name               = var.alb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.alb_sg_id]
  subnets            = var.alb_public_subnet_ids
  tags = var.tags
}

resource "aws_lb_target_group" "alb_webserver_tg" {
  name        = var.alb_tg_name
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id

  tags        = var.tags
}

resource "aws_lb_listener" "webserver_lb_listner" {
  load_balancer_arn = aws_lb.alb_webserver.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_webserver_tg.arn
  }
  tags               = var.tags
}

