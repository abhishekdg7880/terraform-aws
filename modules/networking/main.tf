
#3Private Subnets in 3Az's
resource "aws_subnet" "webserver-private-subnet" {
  count                   = var.local_count
  vpc_id                  = var.vpc_id
  cidr_block              = cidrsubnet(var.vpc_cidr_block, 8, sum([90,"${count.index}"]))
  map_public_ip_on_launch = "false"
  availability_zone       = var.azs[count.index]
  
  tags = var.tags
  
}

#Private Route Table For WebServers Instance1 in AZ1
resource "aws_route_table" "private-route-table" {
  vpc_id = var.vpc_id
  route = []
  tags = var.tags
}

#Route Association with All Private Subnets
resource "aws_route_table_association" "private-webserver-route-association" {
  count          = var.local_count
  subnet_id      = aws_subnet.webserver-private-subnet["${count.index}"].id
  route_table_id = aws_route_table.private-route-table.id
}

#Security Group for LoadBalancer
resource "aws_security_group" "alb_sg" {
  name        = var.alb_sg_name
  description = "Allow incoming HTTP connections"
  vpc_id = var.vpc_id

  ingress {
    from_port        = 80
    protocol         = "tcp"
    to_port          = 80
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags

}

##Security Groups for Nat Instance
resource "aws_security_group" "bastion_nat_sg" {
  name        = var.bastion_instance_sg_name
  description = "Allow SSH to Bastion Instance"
  vpc_id      = var.vpc_id

  ingress {
    description     = "SSH into Bastion Instance"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = [var.bastion_sg_cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

##Security Groups for Private Subnet Instances
resource "aws_security_group" "webservers_sg" {
  name        = var.webservers_sg_name
  description = "Allow SSH to Private Subnets"
  vpc_id      = var.vpc_id

  ingress {
    description     = "Allow TCP access to port 80"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  ingress {
    description     = "SSH From Public Subnet NAT/Bastion Instance"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_nat_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

#VPC S3 Endpoint
resource "aws_vpc_endpoint" "s3_vpc_endpoint" {
  vpc_id            = var.vpc_id
  vpc_endpoint_type = "Gateway"
  service_name      = "com.amazonaws.${var.region}.s3"
  auto_accept       = true
  route_table_ids   = [aws_route_table.private-route-table.id]
  tags              = var.tags
}