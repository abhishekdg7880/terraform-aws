variable "tags" {
    type = map(string)
}

variable "vpc_id" {
    type = string
}

variable "local_count" {
    type = string
}

variable "vpc_cidr_block" {
    type = string
}

variable "azs" {
    type = list(string)
}

variable "alb_sg_name" {
    type = string
}

variable "bastion_instance_sg_name" {
    type = string
}

variable "bastion_sg_cidr_block" {
    type = string
}

variable "webservers_sg_name" {
    type = string
}

variable "region" {
    type = string
}