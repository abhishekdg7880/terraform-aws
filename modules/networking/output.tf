output "aws_alb_security_group_id" {
  value = aws_security_group.alb_sg.id
}

output "aws_bastion_security_group_id" {
  value = aws_security_group.bastion_nat_sg.id
}

output "aws_webservers_security_group_id" {
  value = aws_security_group.webservers_sg.id
}

output "aws_s3_vpc_endpoint_id" {
    value = aws_vpc_endpoint.s3_vpc_endpoint.id
}

output "webserver_private_subnet_ids" {
  value = aws_subnet.webserver-private-subnet.*.id
}
