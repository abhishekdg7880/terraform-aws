#Bastion Host 
resource "aws_instance" "bastion_host" {
  ami                    = var.bastion_ami
  instance_type          = var.bastion_instance_type
  key_name               = var.bastion_key_pair_name
  subnet_id              = var.bastion_subnet_id 
  vpc_security_group_ids = [var.bastion_vpc_security_group_ids]
  source_dest_check      = false
  ebs_optimized          = false

  root_block_device {
    volume_size           = var.bastion_volume_size
    volume_type           = var.bastion_volume_type
    delete_on_termination = true
  }
  tags = var.tags
}
