variable "tags" {
    type = map(string)
}

variable "bastion_ami" {
    type = string
}

variable "bastion_instance_type" {
    type = string
}

variable "bastion_key_pair_name" {
    type = string
}

variable "bastion_subnet_id" {
    type = string
}

variable "bastion_vpc_security_group_ids" {
    type = string
}

variable "bastion_volume_size" {
    type =string
}

variable "bastion_volume_type" {
    type = string
}