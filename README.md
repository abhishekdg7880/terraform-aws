Terraform IAC AWS

Modularisation of Terraform code is made for the below reasons:-
1. Reusability 
2. Organization
3. Collaboration 
4. Testing/Isolation
5. Versioning


The Architecture needed a Bastion Host to ssh into the 
Private WebServers for quick troubleshooting steps, thereby accessing the Bastion Instance can be restricted by making use of corporate VPN and whitelisting the IP Address of the users in the Bastion Security Group.

Also the Private Webservers have an inbound Traffic on port 22 for enabling the Bastion Host to perform an SSH into the private Webserver Instances.

For the Architecture I have made use of a VPC EndPoint to direct the traffic to the S3 bucket using the capability of AWS Route Table, an Interface Endpoint would be recommended keeping in mind the security Best Practices as Interface Endpoints are under the VPC.


Also on the Application Load Balancer one can enable the access logs attributes for capturing the requests made to the ELB for the below reasons:-

1. Debugging and troubleshooting
2. Performance analysis 
3. Security
4. Compliance 
5. Billing and cost optimization

Further more we can enable the Cloudwatch monitoring system for the Private Webservers Instances for capturing network performance of the EC2's and other resources etc.

Since , the application was only a static index.html we don't really need to export any application logs to ELK or splunk , but for any other applications it's recommended to export the logs of the application to Splunk or ELK.


NOTE

1. Since I'm using a Free version of GitLab , the runners didn't worked as expected, thereby i have made use of a Bash Script to perform the same operation.

2. On the CICD part it's recommended to make use of Open Identity Connect to deploy or make any API call to the Cloud Providers.

3. FurtherMore on the CICD part made use of Gitlab rules to control the flow of a particular Job run.

