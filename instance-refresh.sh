#!/bin/bash

ASG_NAME=$1
region=$2

aws autoscaling start-instance-refresh --auto-scaling-group-name $ASG_NAME --preferences {"InstanceWarmup": 300, "MinHealthyPercentage": 50} --region $region
